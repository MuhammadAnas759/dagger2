package com.mdi.dagger2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.mdi.dagger2.dagger.module.NetworkModule
import com.mdi.dagger2.dagger.module.SessionHelper
import javax.inject.Inject

class MainActivity : AppCompatActivity() {


 @set:Inject
lateinit var retrofit: NetworkModule.Retrofit
@set:Inject
lateinit var sessionHelper: SessionHelper


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        

    }
}