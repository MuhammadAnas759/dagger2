package com.mdi.dagger2.dagger.module

import android.util.Log
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetworkModule(@Named("base")var mBaseUrl: String) {


    @Provides
    @Singleton
    fun provideOkhttpClient(): OkHttpClient {
        Log.d("Dagger2","getting okHttpClient")
        return OkHttpClient(" ")
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient?): Retrofit {
        return Retrofit(this.mBaseUrl)
    }

    data class Retrofit(val url:String){
        fun getRetrofit(){
            Log.d("Dagger2","creating Retrofit2")
        }
    }
    data class OkHttpClient(val http:String)
}