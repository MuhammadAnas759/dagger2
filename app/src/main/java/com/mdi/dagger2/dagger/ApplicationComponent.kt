package com.mdi.dagger2.dagger

import android.app.Application
import com.mdi.dagger2.MainActivity
import com.mdi.dagger2.dagger.module.AppModule
import com.mdi.dagger2.dagger.module.NetworkModule
import dagger.BindsInstance
import dagger.Component
import javax.inject.Named
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkModule::class,AppModule::class])
interface ApplicationComponent {

    fun injectMainActivity(activity: MainActivity?)

//    @Component.Builder
    //    abstract class Builder extends AndroidInjector.Builder<ApplicationManager> {
    //
    //    }
//        @Component.Builder
//interface Builder {
//    @BindsInstance
//    fun setmApplication( application: Application):Builder
//    @BindsInstance
//    fun setBaseUrl( @Named("base")url: String):Builder
//    fun build():ApplicationComponent
//}
}