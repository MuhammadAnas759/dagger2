package com.mdi.dagger2

import android.app.Activity
import android.app.Application
import com.mdi.dagger2.dagger.ApplicationComponent
import com.mdi.dagger2.dagger.DaggerApplicationComponent
import com.mdi.dagger2.dagger.module.AppModule
import com.mdi.dagger2.dagger.module.NetworkModule
import com.mdi.dagger2.dagger.module.UtilityModule

class ApplicationManager:Application() {
    private val TAG = "ApplicationManager"
    var component: ApplicationComponent? = null
    val BASEURL:String="www.test.com"


    override fun onCreate() {
        super.onCreate()

        component = DaggerApplicationComponent.builder()
            .appModule(AppModule(this))
            .networkModule(NetworkModule(BASEURL))
            .utilityModule(UtilityModule())
            .build()



    }

    fun getAppComponent(): ApplicationComponent? {
        return component
    }
}