package com.mdi.dagger2.dagger.module

import android.app.Application
import android.util.Log
import dagger.Module
import dagger.Provides
import javax.inject.Inject
import javax.inject.Singleton

//@Module
//class UtilityModule {
//
//    @Provides
//    @Singleton
//    fun provideSessionManager(): SessionHelper {
//        Log.d("Dagger2","getting SessionManager")
//        return SessionHelper("a")
//    }
//}
data class SessionHelper @Inject constructor (val a:String){
    fun getSession():String{
        return "session"
    }
}