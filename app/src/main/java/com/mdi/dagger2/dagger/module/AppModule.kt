package com.mdi.dagger2.dagger.module

import android.app.Application
import android.util.Log
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
 class AppModule( val application: Application) {


        @Provides
        @Singleton
        fun provideApplication(): Application {
            Log.d("Dagger2","getting Application")
            return this.application
        }

}