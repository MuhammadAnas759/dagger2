package com.mdi.dagger2.dagger.module

import com.mdi.dagger2.dagger.RequestComponent
import dagger.Component
import dagger.Module
import javax.inject.Singleton

@Module(subcomponents = [RequestComponent::class])
class ServerModule
@Singleton
@Component(modules = [ServerModule::class])
interface ServerComponent {
    val requestRouter: String
}